function getTheme(themeName) {
  const themes = {
    "modern": {
      "--fretboard-border-size": "0px",
      "--fretboard-border-color": "black",
      "--selected-color": "#27ae60",
      "--tonic-txt-color": "black",
      "--tonic-bg-color": "#16a085",
      "--cell-txt-color": "black",
      "--cell-bg-color": "#2c3e50",
      "--cell-border-color": "black",
      "--ctrls-bg-color": "#2c3e50",
      "--ctrls-bg-color-hover": "#34495e",
      "--select-bg-color": "#95a5a6",
      "--select-bg-color-hover": "#bdc3c7"
    },

    "electric": {
      "--fretboard-border-size": "0px",
      "--fretboard-border-color": "black",
      "--selected-color": "#FFB300",
      "--tonic-txt-color": "black",
      "--tonic-bg-color": "#d2dae2",
      "--cell-txt-color": "black",
      "--cell-bg-color": "#1e272e",
      "--cell-border-color": "black",
      "--ctrls-bg-color": "#1e272e",
      "--ctrls-bg-color-hover": "#485460",
      "--select-bg-color": "#d2dae2",
      "--select-bg-color-hover": "white"
    },

    "wood": {
      "--fretboard-border-size": "0.1em",
      "--fretboard-border-color": "#9c5d47",
      "--selected-color": "#f0aa70",
      "--tonic-txt-color": "black",
      "--tonic-bg-color": "#f2dabf",
      "--cell-txt-color": "black",
      "--cell-bg-color": "#422e2d",
      "--cell-border-color": "#9c5d47",
      "--ctrls-bg-color": "#422e2d",
      "--ctrls-bg-color-hover": "#9c5d47",
      "--select-bg-color": "#d2dae2",
      "--select-bg-color-hover": "white"
    }
  }
  return themes[themeName]
}

function getDefaultNotes() {
  return ["C", "D", "E", "F", "G", "A", "B"]
}

function getDefaultAlts() {
  return ["flat", "natural", "sharp"]
}

function getDefaultChords() {
  return [
    "maj",
    "min",
    "maj7",
    "7",
    "min7",
    "aug",
    "dim",
    "augMaj7",
    "aug7",
    "m7b5",
    "dim7",
  ]
}

function getDefaultScales() {
  return [
    "major",
    "dorian",
    "phrygian",
    "lydian",
    "mixolydian",
    "minor",
    "locrian",
    "pentaMajor",
    "pentaMinor",
    "harmMajor",
    "harmMinor",
    "meloMinor",
  ]
}

export {
  getTheme,
  getDefaultNotes,
  getDefaultAlts,
  getDefaultChords,
  getDefaultScales
}
