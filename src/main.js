import { Howl, Howler } from 'howler'
import Vue from 'vue'
import App from './App'

import sf00 from './sounds/00-E.webm'
import sf01 from './sounds/01-F.webm'
import sf02 from './sounds/02-Gb.webm'
import sf03 from './sounds/03-G.webm'
import sf04 from './sounds/04-Ab.webm'
import sf05 from './sounds/05-A.webm'
import sf06 from './sounds/06-Bb.webm'
import sf07 from './sounds/07-B.webm'
import sf08 from './sounds/08-C.webm'
import sf09 from './sounds/09-Db.webm'
import sf10 from './sounds/10-D.webm'
import sf11 from './sounds/11-Eb.webm'
import sf12 from './sounds/12-E.webm'
import sf13 from './sounds/13-F.webm'
import sf14 from './sounds/14-Gb.webm'
import sf15 from './sounds/15-G.webm'
import sf16 from './sounds/16-Ab.webm'
import sf17 from './sounds/17-A.webm'
import sf18 from './sounds/18-Bb.webm'
import sf19 from './sounds/19-B.webm'
import sf20 from './sounds/20-C.webm'
import sf21 from './sounds/21-Db.webm'
import sf22 from './sounds/22-D.webm'
import sf23 from './sounds/23-Eb.webm'
import sf24 from './sounds/24-E.webm'
import sf25 from './sounds/25-F.webm'
import sf26 from './sounds/26-Gb.webm'
import sf27 from './sounds/27-G.webm'
import sf28 from './sounds/28-Ab.webm'
import sf29 from './sounds/29-A.webm'
import sf30 from './sounds/30-Bb.webm'
import sf31 from './sounds/31-B.webm'
import sf32 from './sounds/32-C.webm'
import sf33 from './sounds/33-Db.webm'
import sf34 from './sounds/34-D.webm'
import sf35 from './sounds/35-Eb.webm'

Vue.prototype.$sounds = []
Vue.prototype.$sounds[0] = new Howl({ src: [sf00] })
Vue.prototype.$sounds[1] = new Howl({ src: [sf01] })
Vue.prototype.$sounds[2] = new Howl({ src: [sf02] })
Vue.prototype.$sounds[3] = new Howl({ src: [sf03] })
Vue.prototype.$sounds[4] = new Howl({ src: [sf04] })
Vue.prototype.$sounds[5] = new Howl({ src: [sf05] })
Vue.prototype.$sounds[6] = new Howl({ src: [sf06] })
Vue.prototype.$sounds[7] = new Howl({ src: [sf07] })
Vue.prototype.$sounds[8] = new Howl({ src: [sf08] })
Vue.prototype.$sounds[9] = new Howl({ src: [sf09] })
Vue.prototype.$sounds[10] = new Howl({ src: [sf10] })
Vue.prototype.$sounds[11] = new Howl({ src: [sf11] })
Vue.prototype.$sounds[12] = new Howl({ src: [sf12] })
Vue.prototype.$sounds[13] = new Howl({ src: [sf13] })
Vue.prototype.$sounds[14] = new Howl({ src: [sf14] })
Vue.prototype.$sounds[15] = new Howl({ src: [sf15] })
Vue.prototype.$sounds[16] = new Howl({ src: [sf16] })
Vue.prototype.$sounds[17] = new Howl({ src: [sf17] })
Vue.prototype.$sounds[18] = new Howl({ src: [sf18] })
Vue.prototype.$sounds[19] = new Howl({ src: [sf19] })
Vue.prototype.$sounds[20] = new Howl({ src: [sf20] })
Vue.prototype.$sounds[21] = new Howl({ src: [sf21] })
Vue.prototype.$sounds[22] = new Howl({ src: [sf22] })
Vue.prototype.$sounds[23] = new Howl({ src: [sf23] })
Vue.prototype.$sounds[24] = new Howl({ src: [sf24] })
Vue.prototype.$sounds[25] = new Howl({ src: [sf25] })
Vue.prototype.$sounds[26] = new Howl({ src: [sf26] })
Vue.prototype.$sounds[27] = new Howl({ src: [sf27] })
Vue.prototype.$sounds[28] = new Howl({ src: [sf28] })
Vue.prototype.$sounds[29] = new Howl({ src: [sf29] })
Vue.prototype.$sounds[30] = new Howl({ src: [sf30] })
Vue.prototype.$sounds[31] = new Howl({ src: [sf31] })
Vue.prototype.$sounds[32] = new Howl({ src: [sf32] })
Vue.prototype.$sounds[33] = new Howl({ src: [sf33] })
Vue.prototype.$sounds[34] = new Howl({ src: [sf34] })
Vue.prototype.$sounds[35] = new Howl({ src: [sf35] })


Vue.config.productionTip = false

new Vue({
  el: '#app',
  render: h => h(App)
})
